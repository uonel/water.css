---
"water.css": patch
---

Added space around scrollbar so it is not against the window edge and is easier to click.
